package com.example.demo.repo;

import com.example.demo.model.Curso;

public interface ICursoRepo extends IGenericRepo<Curso, String>{

}
