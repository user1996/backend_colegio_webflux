package com.example.demo.repo;

import com.example.demo.model.Estudiante;

public interface IEstudianteRepo  extends IGenericRepo<Estudiante, String>{
	
}
