package com.example.demo.repo;

import com.example.demo.model.Usuario;

import reactor.core.publisher.Mono;

public interface IUsuarioRepo extends IGenericRepo<Usuario, String> {

	Mono<Usuario> findOneByUsuario(String usuario);

	Mono<Usuario> findByUsuario(String usuario);
}
