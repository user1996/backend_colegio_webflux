package com.example.demo.repo;

import com.example.demo.model.Rol;

public interface IRolRepo extends IGenericRepo<Rol, String> {

}
