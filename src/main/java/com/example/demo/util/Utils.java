package com.example.demo.util;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.example.demo.model.Curso;

public class Utils {

	public static Integer edadActual(Integer añoNac) {
		return LocalDate.now().getYear() - añoNac;
	}

	public static List<String> cursos(List<Curso> cursos) {
		List<String> lista = new ArrayList<>();
		cursos.forEach(c -> {
			lista.add(c.getNombreCurso());
		});
		return lista;
	}
}
