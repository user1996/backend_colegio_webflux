package com.example.demo.model;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Document(collection = "matriculas")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Matricula {
	@Id
	private String id;
	private LocalDateTime fechaRegistro=LocalDateTime.now();
	private Estudiante estudiante;
	private List<Curso> cursos;
	private Usuario usuario;
	private Boolean estado;
}
