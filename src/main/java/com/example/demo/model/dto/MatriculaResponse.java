package com.example.demo.model.dto;

import java.time.LocalDateTime;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class MatriculaResponse {

	private String id;
	private String documento;
	private String estudiante;
	private LocalDateTime fecha;
	private String registradoPor;
	private List<String> cursos;
	
}
