package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Document("roles")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Rol {
	@Id
	private String id;
	private String nombre;

}
