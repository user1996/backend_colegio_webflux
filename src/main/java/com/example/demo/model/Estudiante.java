package com.example.demo.model;

import java.time.LocalDate;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Document(collection  = "estudiantes")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Estudiante {
	@Id
	private String id;
	@Size(message = "Max 15 - Min 1",max = 15,min = 1)
	private String nombres;
	private String apellidos;
	private String genero;
	private String dni;
	private LocalDate fechaNacimiento;
	@Transient
	private Long edad;
}
