package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.Setter;

@Document(collection = "cursos")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Curso {
	@Id
	private String id;
	private String nombreCurso;
	private String siglas;
	private Double precio;
	private Boolean estado;
}
