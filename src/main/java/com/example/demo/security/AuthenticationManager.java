package com.example.demo.security;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import reactor.core.publisher.Mono;

@Component
public class AuthenticationManager implements ReactiveAuthenticationManager {

	@Autowired
	private JWTUtil jwutil;

	@Override
	public Mono<Authentication> authenticate(Authentication authentication) {
		String tokem = authentication.getCredentials().toString();
		String usuario;
		try {
			usuario = jwutil.getUsernameFromToken(tokem);
		} catch (Exception e) {
			usuario = null;
		}
		if (usuario != null && jwutil.validateToken(tokem)) {
			Claims claims = jwutil.getAllClaimsFromToken(tokem);
			List<String> rolesMap = claims.get("roles", List.class);
			UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(usuario, null, rolesMap
					.stream().map(authoriry -> new SimpleGrantedAuthority(authoriry)).collect(Collectors.toList()));
			return Mono.just(auth);
		} else {
			return Mono.empty();
		}
	}

}
