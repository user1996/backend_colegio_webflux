package com.example.demo.controller;

import java.net.URI;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Curso;
import com.example.demo.model.Matricula;
import com.example.demo.model.Usuario;
import com.example.demo.model.dto.MatriculaResponse;
import com.example.demo.service.ICursoService;
import com.example.demo.service.IEstudianteService;
import com.example.demo.service.IMatriculaService;
import com.example.demo.service.IUsuarioService;
import com.example.demo.util.Utils;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/matricula")
public class MatriculaController {
	@Autowired
	IMatriculaService service;
	@Autowired
	ICursoService cursoService;
	@Autowired
	IEstudianteService estudianteService;
	@Autowired
	IUsuarioService usuarioService;
	
	
	@GetMapping
	private Mono<ResponseEntity<Flux<MatriculaResponse>>> listar() {
		return Mono
				.just(ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(
								service.listar().flatMap(mat->{
								List<Curso> cursos=new ArrayList<>();
								return Flux.fromIterable(mat.getCursos()).flatMap(curso->{
									return cursoService.listarPorId(curso.getId()).map(c->{
										cursos.add(c);
										return c;
									});
								}).collectList().flatMap(list->{
									mat.setCursos(cursos);
									return Mono.just(mat);
								});
							}).flatMap(ma->{
								return usuarioService.listarPorId(ma.getUsuario().getId()).flatMap(user->{
									return estudianteService.listarPorId(ma.getEstudiante().getId()).flatMap(est->{
										ma.setEstudiante(est);
										String estudiante=ma.getEstudiante().getNombres().concat(" ").concat(ma.getEstudiante().getApellidos());
										return Mono.just(new MatriculaResponse(ma.getId(),ma.getEstudiante().getDni(),estudiante,ma.getFechaRegistro(),user.getUsuario(), Utils.cursos(ma.getCursos())));
									});
								});
							})
						));
	}

	@PostMapping
	private Mono<ResponseEntity<Matricula>> registrar(@RequestBody Matricula matricula,Mono<Principal> principal, final ServerHttpRequest req) {
		return principal.map(Principal::getName)
				.flatMap(name->{
					return usuarioService.buscarUnoPorUsuario(name)
							.flatMap(user->{
								Usuario usuario=new Usuario();
								usuario.setId(user.getId());
								matricula.setUsuario(usuario);
								return service.registrar(matricula)
										.map(est -> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(est.getId())))
												.contentType(MediaType.APPLICATION_JSON).body(est));
							});
				});
	}

	@PutMapping("/{id}")
	private Mono<ResponseEntity<Matricula>> modificar(@PathVariable String id, @RequestBody Matricula matricula) {
		return service.listarPorId(id).flatMap(mat -> {
			return service.modificar(matricula)
					.map(m -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(m));
		}).defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@GetMapping("/{id}")
	private Mono<ResponseEntity<Matricula>> buscarPorId(@PathVariable String id) {
		return service.listarPorId(id)
				/**
				.flatMap(mat->{
					return estudianteService.listarPorId(mat.getEstudiante().getId())
							.flatMap(est->{
								List<Curso> cursos=new ArrayList<>();
								mat.setEstudiante(est);
								return Flux.fromIterable(mat.getCursos())
										.flatMap(cur->{
											return cursoService.listarPorId(cur.getId())
													.map(c->{
														cursos.add(c);
														return c;
													});
										}).collectList().flatMap(list->{
											mat.setCursos(cursos);
											return Mono.just(mat);
										});
							});
				})
				**/
				.map(matricula -> ResponseEntity.ok().body(matricula))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	@GetMapping("/detalle/{id}")
	private Mono<ResponseEntity<Matricula>> buscarDetallePorId(@PathVariable String id) {
		return service.listarPorId(id)
				.flatMap(mat->{
					return estudianteService.listarPorId(mat.getEstudiante().getId())
							.flatMap(est->{
								List<Curso> cursos=new ArrayList<>();
								mat.setEstudiante(est);
								return Flux.fromIterable(mat.getCursos())
										.flatMap(cur->{
											return cursoService.listarPorId(cur.getId())
													.map(c->{
														cursos.add(c);
														return c;
													});
										}).collectList().flatMap(list->{
											mat.setCursos(cursos);
											return Mono.just(mat);
										});
							});
				})
				.map(matricula -> ResponseEntity.ok().body(matricula))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@DeleteMapping("/{id}")
	private Mono<ResponseEntity<Void>> eliminarPorId(@PathVariable String id) {
		return service.listarPorId(id).flatMap(est -> {
			return service.eliminar(est.getId()).then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
		}).defaultIfEmpty(ResponseEntity.notFound().build());
	}
}
