package com.example.demo.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Curso;
import com.example.demo.service.ICursoService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/curso")
public class CursoController {
	@Autowired
	ICursoService service;

	@GetMapping
	private Mono<ResponseEntity<Flux<Curso>>> listar() {
		return Mono
				.just(ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
				.body(service.listar()));
	}
	@PostMapping
	private Mono<ResponseEntity<Curso>> registrar(@RequestBody Curso curso,final ServerHttpRequest req) {
		curso.setEstado(true);
		return service.registrar(curso)
				.map(cur->ResponseEntity
						.created(URI.create(req.getURI().toString().concat("/").concat(cur.getId())))
						.contentType(MediaType.APPLICATION_JSON)
						.body(cur));
	}
	@PutMapping("/{id}")
	private Mono<ResponseEntity<Curso>> modificar(@PathVariable String  id, @RequestBody Curso curso) {
		return service.listarPorId(id).flatMap(est->{
			return service.modificar(curso)
			.map(cur->ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(cur));
		}).defaultIfEmpty(ResponseEntity.notFound().build());
	}
	@GetMapping("/cambiaEstado/{id}/{estado}")
	private Mono<ResponseEntity<Curso>> cambiaEstado(@PathVariable String  id, @PathVariable Boolean estado) {
		return service.listarPorId(id).flatMap(est->{
			est.setEstado(estado);
			return service.modificar(est)
			.map(cur->ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(cur));
		}).defaultIfEmpty(ResponseEntity.notFound().build());
	}
	@GetMapping("/{id}")
	private Mono<ResponseEntity<Curso>> buscarPorId(@PathVariable String id) {
		return service.listarPorId(id)
				.map(cur->ResponseEntity.ok().body(cur))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	@DeleteMapping("/{id}")
	private Mono<ResponseEntity<Void>> eliminarPorId(@PathVariable String id){
		return service.listarPorId(id)
				.flatMap(cur->{
					return service.eliminar(cur.getId())
							.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
					}).defaultIfEmpty(ResponseEntity.notFound().build());
	}
}

