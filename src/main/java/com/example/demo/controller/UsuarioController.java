package com.example.demo.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Usuario;
import com.example.demo.service.IUsuarioService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
	@Autowired
	IUsuarioService service;

	@GetMapping
	private Mono<ResponseEntity<Flux<Usuario>>> listar() {
		return Mono
				.just(ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
				.body(service.listar()));
	}
	@PostMapping
	private Mono<ResponseEntity<Usuario>> registrar(@RequestBody Usuario usuario,final ServerHttpRequest req) {
		return service.registrar(usuario)
				.map(cur->ResponseEntity
						.created(URI.create(req.getURI().toString().concat("/").concat(cur.getId())))
						.contentType(MediaType.APPLICATION_JSON)
						.body(cur));
	}
	@PutMapping("/{id}")
	private Mono<ResponseEntity<Usuario>> modificar(@PathVariable String  id, @RequestBody Usuario Usuario) {
		return service.listarPorId(id).flatMap(est->{
			return service.modificar(Usuario)
			.map(cur->ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(cur));
		}).defaultIfEmpty(ResponseEntity.notFound().build());
	}
	@GetMapping("/{id}")
	private Mono<ResponseEntity<Usuario>> buscarPorId(@PathVariable String id) {
		return service.listarPorId(id)
				.map(cur->ResponseEntity.ok().body(cur))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	@DeleteMapping("/{id}")
	private Mono<ResponseEntity<Void>> eliminarPorId(@PathVariable String id){
		return service.listarPorId(id)
				.flatMap(cur->{
					return service.eliminar(cur.getId())
							.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
					}).defaultIfEmpty(ResponseEntity.notFound().build());
	}
}

