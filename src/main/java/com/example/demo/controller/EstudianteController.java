package com.example.demo.controller;

import java.net.URI;
import java.security.Principal;
import java.util.Comparator;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Estudiante;
import com.example.demo.service.IEstudianteService;
import com.example.demo.util.Utils;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/estudiante")
public class EstudianteController {
	@Autowired
	IEstudianteService service;

	@GetMapping("/current")
	public Mono<String> hello(Mono<Principal> principal) {
		return principal
			.map(p->{
				return p.getName();
			});
	}
	
	@GetMapping
	private Mono<ResponseEntity<Flux<Estudiante>>> listar(Mono<Principal> principal) {
		Flux<Estudiante> f_estudiante=service.listar().flatMap(estu->{
			estu.setEdad((long)Utils.edadActual(estu.getFechaNacimiento().getYear()));
			return Mono.just(estu);
		})
		.sort(Comparator.comparing(Estudiante::getEdad));		
		return Mono
				.just(ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
				.body(f_estudiante));
	}
	@PostMapping
	private Mono<ResponseEntity<Estudiante>> registrar(@RequestBody Estudiante estudiante,final ServerHttpRequest req) {
		return service.registrar(estudiante)
				.flatMap(estu->{
					estu.setEdad((long)Utils.edadActual(estu.getFechaNacimiento().getYear()));
					return Mono.just(estu);
				})
				.map(est->ResponseEntity
						.created(URI.create(req.getURI().toString().concat("/").concat(est.getId())))
						.contentType(MediaType.APPLICATION_JSON)
						.body(est));
	}
	@PutMapping("/{id}")
	private Mono<ResponseEntity<Estudiante>> modificar(@PathVariable String  id,@Valid @RequestBody Estudiante estudiante) {
		return service.listarPorId(id).flatMap(est->{
			return service.modificar(estudiante)
			.flatMap(estu->{
				estu.setEdad((long)Utils.edadActual(estu.getFechaNacimiento().getYear()));
					return Mono.just(estu);
			})
			.map(e->ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(e));
		}).defaultIfEmpty(ResponseEntity.notFound().build());
	}
	@GetMapping("/{id}")
	private Mono<ResponseEntity<Estudiante>> buscarPorId(@PathVariable String id) {
		return service.listarPorId(id)
				.map(est->ResponseEntity.ok().body(est))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	@DeleteMapping("/{id}")
	private Mono<ResponseEntity<Void>> eliminarPorId(@PathVariable String id){
		return service.listarPorId(id)
				.flatMap(est->{
					return service.eliminar(est.getId())
							.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
					}).defaultIfEmpty(ResponseEntity.notFound().build());
	}
}
