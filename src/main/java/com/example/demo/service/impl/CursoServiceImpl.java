package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Curso;
import com.example.demo.repo.ICursoRepo;
import com.example.demo.repo.IGenericRepo;
import com.example.demo.service.ICursoService;
@Service
public class CursoServiceImpl extends CRUDImpl<Curso, String> implements ICursoService {

	@Autowired
	ICursoRepo repo;
	
	@Override
	protected IGenericRepo<Curso, String> getRepo() {
		return repo;
	}

}
