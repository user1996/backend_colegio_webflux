package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Rol;
import com.example.demo.repo.IGenericRepo;
import com.example.demo.repo.IRolRepo;
import com.example.demo.service.IRolService;

@Service
public class RolServiceImpl extends CRUDImpl<Rol, String> implements IRolService {

	@Autowired
	private IRolRepo repo;
	@Override
	protected IGenericRepo<Rol, String> getRepo() {
		return repo;
	}

}
