package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Matricula;
import com.example.demo.repo.IGenericRepo;
import com.example.demo.repo.IMatriculaRepo;
import com.example.demo.service.IMatriculaService;

@Service
public class MatriculaServiceImpl extends CRUDImpl<Matricula, String> implements IMatriculaService{

	@Autowired
	IMatriculaRepo repo;
	@Override
	protected IGenericRepo<Matricula, String> getRepo() {
		return repo;
	}

}
