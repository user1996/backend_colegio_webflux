package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.model.Usuario;
import com.example.demo.repo.IGenericRepo;
import com.example.demo.repo.IRolRepo;
import com.example.demo.repo.IUsuarioRepo;
import com.example.demo.security.User;
import com.example.demo.service.IUsuarioService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UsuarioServiceImpl extends CRUDImpl<Usuario, String> implements IUsuarioService {

	@Autowired
	private IUsuarioRepo repo;
	
	@Autowired
	private IRolRepo repoRol;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	protected IGenericRepo<Usuario, String> getRepo() {
		return repo;
	}

	@Override
	public Mono<User> buscarPorUsuario(String usuario) {
		Mono<Usuario> monoUsuario = repo.findOneByUsuario(usuario);
		List<String> roles = new ArrayList<String>();
		return monoUsuario.flatMap(u -> {
			return Flux.fromIterable(u.getRoles()).flatMap(rol -> {
				return repoRol.findById(rol.getId()).map(r -> {
					roles.add(r.getNombre());
					return r;
				});
			}).collectList().flatMap(list -> {
				u.setRoles(list);
				return Mono.just(u);
			});
		}).flatMap(u -> {
			return Mono.just(new User(u.getUsuario(), u.getClave(), u.getEstado(), roles));
		});
	}
	@Override
	public Mono<Usuario> registrar(Usuario usuario) {
		usuario.setClave(passwordEncoder.encode(usuario.getClave()));
		return repo.save(usuario);
	}
	@Override
	public Mono<Usuario> modificar(Usuario usuario) {
		usuario.setClave(passwordEncoder.encode(usuario.getClave()));
		return getRepo().save(usuario);
	}

	@Override
	public Mono<Usuario> buscarUnoPorUsuario(String usuario) {
		return repo.findOneByUsuario(usuario);
	}

}
