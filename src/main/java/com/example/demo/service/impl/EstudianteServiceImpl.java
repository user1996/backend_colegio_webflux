package com.example.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Estudiante;
import com.example.demo.repo.IEstudianteRepo;
import com.example.demo.repo.IGenericRepo;
import com.example.demo.service.IEstudianteService;
@Service
public class EstudianteServiceImpl extends CRUDImpl<Estudiante, String> implements IEstudianteService{

	@Autowired
	IEstudianteRepo repo;
	@Override
	protected IGenericRepo<Estudiante, String> getRepo() {
		return repo;
	}

}
