package com.example.demo.service;

import com.example.demo.model.Usuario;
import com.example.demo.security.User;

import reactor.core.publisher.Mono;

public interface IUsuarioService extends ICRUD<Usuario, String> {

	Mono<User> buscarPorUsuario(String usuario);
	
	Mono<Usuario> buscarUnoPorUsuario(String usuario);
	
}
